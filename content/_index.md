---
title: "Home"
date: 2018-02-10T18:56:13-05:00
sitemap:
  priority : 1.0

outputs:
- html
- rss
- json
---
Experienced Cloud Enabler. Building not only cloud-based solutions, but also ways of working focused on day-two operations. Strong believer in Infrastructure-as-Code. 
